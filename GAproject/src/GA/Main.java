package GA;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
/******************************************
 * 
 * @author Mitchell Wilcox
 *	Genetic Alogrithm test
 *	Iterative Prisoners Dilemma 
 *	Binary chromosomes 
 */
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ToolBox tb = new ToolBox(16,500,2,0.5f,0.1f);
		
		//System.out.println("get as decimal: " + tb.getDecimalAsInt(tb.round(d, 2)));
		
		for(int i=0;i<10;i++) {
			//tb.printFitness();
			tb.Generation();
			tb.printNumOfChromos();
		}
		tb.printFittestChromo();
	}

}

class ToolBox{
	int numBits, population, memDepth;
	float crossoverProb, mutationProb;
	
	Chromosome[] chromoList;
	/**
	 * constructor for the toolbox 
	 * calls create population method
	 * @param _nb = number of bits
	 * @param _pop = population limit
	 * @param _md = memory depth
	 * @param _cp = crossover probability as decimal
	 * @param _mp = mutation probability as decimal
	 */
	public ToolBox(int _nb, int _pop, int _md, float _cp, float _mp) {
		chromoList = new Chromosome[_pop];
		
		numBits = _nb;
		population = _pop;
		memDepth = _md;
		crossoverProb = _cp;
		mutationProb = _mp;
		
		CreatePopulation();
	}
	/***************************************************************************************************/
	/**
	 * @return random 0,1
	 */
	private int ranBit() {
		Random ran = new Random();
		return ran.nextInt(2);
	}
	/**
	 * fill chromo list with Chromosomes objects 
	 * chromosomes have random bits 0-1 
	 */
	private void CreatePopulation(){
		int[] tmpChromo = new int[numBits];
		for(int i=0; i<population;i++) {
			for(int y=0; y<numBits;y++) {
				tmpChromo[y] = ranBit();
			}
			chromoList[i] = new Chromosome(tmpChromo,0);
			tmpChromo = new int[numBits];
		}
	}
	/***************************************************************************************************/
	/**
	 * average fitness of all chromosomes
	 * @return int
	 */
	private int getAverageFitness() {
		int averageFitness = 0;
		for(int i=0;i<population;i++)
			averageFitness += chromoList[i].fitness;
		System.out.println("avrg: " + averageFitness + "pop: " + population);
		return averageFitness / population;
	}
	/***************************************************************************************************/
	/**
	 * loop function, loops through population and test chromosomes 
	 * against each other
	 * complength = how many moves each chromo can make
	 * gets index from move string
	 */
	void FitnessFunction() {
		String moves = "00";
		int tmpFitness = 0;
		int compLength = 10;
		
		for(int ind=0; ind<population;ind++) {//current individual being tested
			
			for(int compInd = 0; compInd< population; compInd++) {//current opponent being tested
				for(int m=0;m<compLength;m++) {
					int tmpIndex = returnIndex(moves);//store the index for strat
					int playerMove = lookUpStrat(ind,tmpIndex);//players next move
					int oppMove = lookUpStrat(compInd,tmpIndex);//opponents next move
					moves += playerMove;//record players next move
					moves += oppMove;//records opponents next move
					
					tmpFitness = decisionTable(playerMove, oppMove);// get the score from the move set
					if(moves.length()>memDepth*2)//if the history is longer than the memDepth * 2
						moves = moves.substring(2);//remove the first 2 moves
					
				}
				chromoList[ind].plusFitness(tmpFitness);//add the fitness to the chromosomes
				moves = "00";//reset moves
				tmpFitness = 0;//reset tmp fitness
			}
		}
	}
	/**
	 * return moves from binary to in index
	 * @param moves
	 * @return int index
	 */
	int returnIndex(String moves) {
		Integer i = Integer.parseInt(moves,2);
		return (int)i;
	}
	/**
	 * gets the bit form the chromosome
	 * @param chromo
	 * @param index
	 * @return int bit
	 */
	int lookUpStrat(int chromo,int index) {
		return chromoList[chromo].getBit(index);
	}
	/**
	 * takes both players moves and returns the score for that move set
	 * @param p = player move
	 * @param o = opponent move
	 * @return int score
	 */
	int decisionTable(int p, int o) {
		if(p == 0 && o == 0)//if 0,0 score = 3
			return 3;
		else if(p == 1 && o == 0)//if 1,0 score = 5
			return 5;
		else if(p==0&&o==1)//if 0,1 score = 0
			return 0;
					
		return 1;//if 1,1 score = 1
	}
	/***************************************************************************************************/
	/**
	 * Evaluation Function
	 * tmp score is current chromosome fitness / the average fitness
	 * 
	 * if the tmp score is greater than 1 and 
	 * a random num(0-100) less than the deciaml of tmp score 
	 * 
	 * add the chromosome to the tmp list for each whole num in tmp score 
	 */
	void EvaluationFunction() {
		double avgFit = (double)getAverageFitness();// average fitness of the population
		System.out.println("average: "+ avgFit);
		List<Chromosome> chromosToMate = new ArrayList<Chromosome>();//tmp list of successful chromos
		List<Chromosome> leftOverChromos = new ArrayList<Chromosome>();
		
		for(int pop = 0;pop < population;pop++) {//each individual in the pop
			//tmpScore round current chromo fitness / the average fitness
			float tmpScore = round((chromoList[pop].getFitness()/avgFit),2);
			Random ran = new Random();
			
			//tmpScore > 1 and ran 0-100 < tmpScore and int  
			int z = ran.nextInt(100);
			//System.out.println("tmpScore: " + tmpScore);
			if(tmpScore > 1) {
				//add the successful chromos to the tmp list for each score over 1 
				chromosToMate.add(chromoList[pop]);
				if(z < getDecimalAsInt(tmpScore))
					chromosToMate.add(chromoList[pop]);
			}else{
				leftOverChromos.add(chromoList[pop]);
			}
		}
		System.out.println("tmp chromo list length: " + chromosToMate.size());
		SuccessorFunction(chromosToMate, leftOverChromos);
	}
	/**
	 * take the successful chromosomes and apply crossover and mutation
	 * go through each chromosome in the array and match it with a a random chromosome
	 * 
	 * if successful crossover based on crossoverProb
	 * create 2 new chromosomes 
	 * then apply mutation
	 * 
	 * add to new children list
	 * 
	 * @param tmpChromoList = temp array list
	 */
	void SuccessorFunction(List<Chromosome> chromosToMate, List<Chromosome> leftOverChromos) {
		List<Chromosome> newChromoList = new ArrayList<Chromosome>();
		Random ran = new Random();
		Chromosome tmpChromo1 = new Chromosome(numBits, 0);
		Chromosome tmpChromo2 = new Chromosome(numBits, 0);
		int randNum = 0;
		
		for(int pop=0;pop<chromosToMate.size();pop++) {
			if(ran.nextInt(100) == getDecimalAsInt(crossoverProb)) {
				randNum = ran.nextInt(chromosToMate.size());
				tmpChromo1.fillChromo(Crossover(chromosToMate.get(pop).getChromosome(),
						chromosToMate.get(randNum).getChromosome()));
				
				tmpChromo2.fillChromo(Crossover(chromosToMate.get(randNum).getChromosome(),
						chromosToMate.get(pop).getChromosome()));
				Mutation(tmpChromo1.getChromosome());
				Mutation(tmpChromo2.getChromosome());
				
				newChromoList.add(tmpChromo1);
				newChromoList.add(tmpChromo2);
			}else {
				newChromoList.add(chromosToMate.get(pop));
			}
			tmpChromo1 = new Chromosome(numBits, 0);
			tmpChromo2 = new Chromosome(numBits, 0);
		}
		System.out.println("tmp chromo list length: " + newChromoList.size());
		ReplaceParentsWithChildren(newChromoList, leftOverChromos);
	}
	/**
	 * take the first half of a and the second half of b and create to int array
	 * @param a = int[]
	 * @param b = int[]
	 * @return int[] c
	 */
	int[] Crossover(int[] a, int[] b) {
		int len = a.length;
		int[] c = new int[len];
		int lenHalf = len / 2;
		for(int i=0;i<len;i++) {
			if(i<lenHalf)
				c[i] = a[i];
			else
				c[i] = b[i];
		}
		return c;
	}
	/**
	 * probability to switch each individual bit in a 
	 * with mutation prob
	 * @param a = int[]
	 */
	void Mutation(int[] a) {
		Random ran = new Random();
		for(int i=0;i<a.length;i++)
			if(ran.nextInt(100) == getDecimalAsInt(mutationProb))
				a[i] = switchBit(a[i]);
	}
	/**
	 * switch a in range 0-1
	 * @param a
	 * @return 0 or 1
	 */
	int switchBit(int a) {
		if(a == 0)
			return 1;
		return 0;
	}
	/**
	 * round to x number decimal place
	 * @param d = the number you want rounded
	 * @param decimalPlace = how many decimal places
	 * @return float rounded number
	 */
	float round(double d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Double.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }
	/**
	 * takes a float and returns the decimal part as int
	 * @param n = the decimal you want 
	 * @return deciaml as int
	 */
	int getDecimalAsInt(float n) {
		n = n % 1;
		n = n*100;
		return (int)n;
	}
	/**
	 * takes an array list of children and replaces the 
	 * parent array  
	 * @param tmpList
	 */
	void ReplaceParentsWithChildren(List<Chromosome> chromosToMate, List<Chromosome> leftOverChromos) {
		int newLen = chromosToMate.size()+leftOverChromos.size();
		chromoList = new Chromosome[newLen];
		
		for(int i=0;i<chromosToMate.size();i++)
			chromoList[i] = chromosToMate.get(i);
		
		for(int i=chromosToMate.size();i<leftOverChromos.size();i++)
			chromoList[i] = leftOverChromos.get(i);
		
		population = chromoList.length;
	}
	/***************************************************************************************************/
	/**
	 * Generation public function that calls the system
	 */
	public void Generation() {
		FitnessFunction();
		EvaluationFunction();
	}
	/**
	 * printFittestChromo prints the current fittest chromosome
	 */
	public void printFittestChromo() {
		int fittest = 0;
		int index = 0;
		
		for(int i=0;i<chromoList.length;i++) {
			if(chromoList[i].fitness > fittest){
				index = i;
				fittest = chromoList[i].fitness;
			}
		}
		System.out.println(chromoList[index]);
	}
	public void printFitness() {
		int fittest = 0;
		int index = 0;
		
		for(int i=0;i<chromoList.length;i++) {
			if(chromoList[i].fitness > fittest){
				index = i;
				fittest = chromoList[i].fitness;
			}
		}
		System.out.println("Fittest: " + chromoList[index].fitness);
	}
	public void printNumOfChromos() {
		System.out.println("number of chromosomes: " + chromoList.length);
	}
}

class Chromosome{
	int[] chromosome;
	int fitness;
	
	public Chromosome(int[] _c, int _f) {
		fillChromo(_c);
		fitness = _f;
	}
	public Chromosome(int _nb,int _f) {
		chromosome = new int[_nb];
		fitness = _f;
	}
	public void fillChromo(int[] _c) {
		chromosome = new int[_c.length];
		for(int i =0; i< _c.length;i++)
			chromosome[i]= _c[i];
	}
	
	public int[] getChromosome() {
		return chromosome;
	}
	public int getBit(int index) {
		return chromosome[index];
	}
	public int getFitness() {
		return fitness;
	}
	public void plusFitness(int f) {
		fitness += f;
	}
	public void setFitness(int f) {
		fitness = f;
	}
}
